//
//  LinkButton.swift
//  SwiftUI-List-Starter
//
//  Created by Massimiliano Bonafede on 06/09/22.
//

import SwiftUI

struct LinkButton: View {
    var title: String
    var url: URL
    
    var body: some View {
        Link(destination: url) {
            Text(title)
                .bold()
                .font(.title2)
                .frame(width: 288, height: 50)
                .background(Color(.systemRed))
                .foregroundColor(.white)
                .cornerRadius(10)
        }

    }
}
