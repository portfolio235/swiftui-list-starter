//
//  VideoDetailView.swift
//  SwiftUI-List-Starter
//
//  Created by Massimiliano Bonafede on 06/09/22.
//

import SwiftUI

struct VideoDetailView: View {
    var video: Video
    
    var body: some View {
        VStack(spacing: 20) {
            Spacer()
            
            ViewDetails(
                title: video.title,
                description: video.description,
                image: video.imageName,
                viewCount: video.viewCount,
                uploadDate: video.uploadDate)
            
            Spacer()
            
            LinkButton(title: "Watch now", url: video.url)
        }
    }
}

struct VideoDetailView_Previews: PreviewProvider {
    static var previews: some View {
        VideoDetailView(video: VideoList.topTen.first!)
    }
}
