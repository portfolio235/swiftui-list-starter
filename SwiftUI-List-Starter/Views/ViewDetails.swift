//
//  ViewDetails.swift
//  SwiftUI-List-Starter
//
//  Created by Massimiliano Bonafede on 06/09/22.
//

import SwiftUI

struct ViewDetails: View {
    var title: String
    var description: String
    var image: String
    var viewCount: Int
    var uploadDate: String
    
    var body: some View {
        Image(image)
            .resizable()
            .scaledToFit().frame(height: 150)
            .cornerRadius(12)
        Text(title)
            .font(.title2)
            .fontWeight(.semibold)
            .lineLimit(2)
            .multilineTextAlignment(.center)
            .padding(.horizontal)
        
        HStack(spacing: 40) {
            Label("\(viewCount)", systemImage: "eye.fill")
                .font(.subheadline)
                .foregroundColor(.secondary)
            
            Text(uploadDate)
                .font(.subheadline)
                .foregroundColor(.secondary)
        }
        
        Text(description)
            .font(.body)
            .padding()
    }
}
